+++
title = 'Enlaces de Interés'
date = 2023-11-02T18:56:32+02:00
draft = false
+++

#### Mapa de CyclOSM:

- [Mapa de CyclOSM](https://www.cyclosm.org/#map=13/42.3398/-3.6924/cyclosm), basado en OSM y orientado a bicicletas

#### Consultas en Overpass Turbo:

- [Puntos de autoreparación de bicicletas (Ayuntamiento)](https://overpass-turbo.eu/s/1Dvc)
- [Aparcamientos de bicicletas públicos](https://overpass-turbo.eu/s/1Dvm)
- [Tiendas de bicicletas](https://overpass-turbo.eu/s/1Dvo)
- [Tiendas que alquilan bicis](https://overpass-turbo.eu/s/1Dvt)
- [Tiendas que reparan bicis](https://overpass-turbo.eu/s/1DvG)
- [Tiendas que venden bicis de segunda mano](https://overpass-turbo.eu/s/1DvI)
- [Bicibur](https://overpass-turbo.eu/s/1Dvv)

#### Crear Rutas:

- [BRouter](http://brouter.de/brouter-web/#map=13/42.3480/-3.6749/standard), permite crear rutas para tu bici a partir de datos de OSM
