+++
title = 'Crear una ruta'
date = 2023-10-03T17:39:06+02:00
draft = false
+++

### BRouter

Creamos nuestra ruta en bici usando una aplicación web llamada: [BRouter](http://brouter.de/brouter-web/#map=13/42.3480/-3.6749/standard 'http://brouter.de').

Ejemplo:

[Ruta Museo Evolución/Piscinas del Plantio](http://brouter.de/brouter-web/#map=19/42.33969/-3.69810/Stamen.Terrain&lonlats=-3.698884,42.339824;-3.675892,42.344559)

![ruta centro-piscinas plantio](/images/brouter_ruta_centro_plantio.png)
_Captura de pantalla de la ruta "Museo Evolución - Plantio" realizada con BRouter._

Cuando calcula la ruta nos aparece abajo un resumen con la información básica de esa ruta:

- 2.4 km de distancia
- 7 minutos de duración
- 0.01 kWh energia total
- 9 metros de ascenso
- 3.38 coste / factor de coste medio = 1.42 (mean cost factor)

#### ¿Qué es el Coste y el Factor de coste medio?

BRouter evalua la calidad de la ruta asignando un coste a cada tramo, ese coste es como si se aumentara la distancia de ese tramo en función de lo que cueste hacer esa distancia. En un caso ideal (tramo llano y de asfalto en buenas condiciones) la relación sería 1/1, pero en un tramo con, por ejemplo mucha grava, cuesta mas hacer 1 km que en un tramo de asfalto, o un tramo de subida cuesta más de un tramo de bajada.

### Cambiar el mapa base

Podemos elegir entre varias capas (layers) desde el menu de la derecha:

- OpenStreetMap (mapa base de OpenStreetMap)
- OpenTopoMap (mapa topográfico, con curvas de nivel y otros elementos típicos de estos)
- Terrain (mapa del terreno diseñado por una empresa llamada Stamen a partir de datos de OpenStreetMap)
- Esri Word Imagery (mapa de imagen por satélite propiedad de ESRI)

### Marcar la diferencia de altitud:

Si queremos ver por colores los tramos con mayor altitud, marcando "Altitude Coding" (menu lateral de la izquierda) y pulsando la tecla "C", alternamos la codificación por colores de la ruta:

![ruta centro-piscinas plantio / altitude coding](/images/brouter_ruta_centro_plantio_altitud.png)
_En rojo aparece la subida del tramo del puente de la autovía, Ruta "Museo Evolución - Plantio" realizada con BRouter._

### Grafica de elevación completa

Pulsando el icono de gráfica (abajo a la derecha), o pulsando la tecla "E":

![ruta centro-piscinas plantio](/images/brouter_ruta_centro_plantio_grafica_altitud.png)
_Perfil longitudinal de la ruta con la elevación de cada tramo, Ruta "Museo Evolución - Plantio" realizada con BRouter._

### Información sobre los diferentes tramos que componen esa ruta:

Pulsando la tecla "T" vemos la tabla y al desplazarnos sobre ella vemos datos como:

- elevación
- distancia
- tipo de superficie (asfalto, tierra, etc)
- etiquetas que nos indican si es carril bici, unidireccional si/no, etc

### Personalizar el perfil

En el menu horizontal de la parte de arriba hay un desplegable donde se puede elegir entre los distintos perfiles el que más se ajuste a por donde queremos realizar la ruta:

- treking bike
- bici carretera (fast bike)
- mas corto (shortest)
- etc

Si queremos crear nuestro propio perfil en el icono de una llave inglesa (menu derecha) podemos personalizar alguna de las características de la ruta, por ejemplo:

- avoid_unsafe: para evitar autopistas
- consider_noise: evitar rutas muy ruidosas
- consider_traffic: evitar zonas de mucho tráfico
- peso, velocidad máxima, etc para una mejor estimación de la duración de la ruta

### Analizar la ruta

Pulsando la tecla "T" en el menu de la derecha, nos aparece una tabla donde se divide la ruta por categorias:

- carril bici (toda la parte que va paralela al Parque de la Quinta)
- residencial (Avenida del Arlanzón)
- ruta a pie (parte final donde vamos por la acera)

También se desglosa el tipo de superficie (cuando aparece en OSM), y la suavidad (Smoothness) de la misma.

### App para Android

Existe tambien una app para moviles Android llamada "BRouter Offline Navigation".

### Exportar la ruta

![exportar ruta](/images/brouter_ruta_centro_plantio_export.png)

Si no podeis o quereis usar su app, una buena opción es crear la ruta en vuestro ordenador con la web, exportar el track en .gpx, .geoJSON o el formato que deseeis, y luego importarlo donde se quiera.

Por ejemplo, en [Organic Maps](https://organicmaps.app/es/), mucho mejor que Google Maps u otras, se puede usar offline y está disponible en F-Droid. Ya hablaremos de ella en otro post, pero como dicen en su web:

La aplicación Organic Maps está libre de rastreadores y otras cosas malas

- Sin anuncios
- Sin rastreadores
- Sin recolección de datos
- No se conecta en secreto a ningún servidor
- Sin registros molestos
- Sin tutoriales obligatorios
- Sin spam por correo electrónico
- Sin notificaciones
- Sin componentes basura
- Sin pesticidas ¡Completamente orgánico!
