+++
title = 'Consultas en Overpass Turbo'
date = 2023-11-16T12:57:06+01:00
draft = false
+++

Supongamos que queremos hacer unas pequeñas reparaciones a nuestra bici y necesitamos saber donde están situados los puntos de auto reparación de bicicletas que instaló el Ayuntamiento de Burgos en mayo de 2023.

![punto de autoreparación de bicis](/images/puntoautoreparacionbici.png)
_Punto de autoreparación de bicicletas situado junto a las piscinas municipales de Capiscol._

Utilizaremos una herramienta web llamada [Overpass Turbo](https://wiki.openstreetmap.org/wiki/ES:Overpass_turbo 'https://wiki.openstreetmap.org/wiki/ES:Overpass_turbo'). En ella podemos ejecutar un consulta, a traves de la API de Overpass, a los datos de Openstreetmap y mostrar los resultados sobre un mapa interactivo.

Para buscar los puestos de reparación de bicis tenemos que saber como se llaman estos nodos en la Base de Datos de OpenStretMap, en este caso sabemos que se denominan: [bicycle_repair_station](https://wiki.openstreetmap.org/wiki/Item:Q6244 'https://wiki.openstreetmap.org/wiki/Item:Q6244').

Creamos una consulta en Overpass Turbo que busca nodos que coincidan con la etiqueta elegida dentro de un rectángulo con las coordenadas marcadas (que en este caso se corresponden a Burgos ciudad):

![consulta en Overpass turbo](/images/overpassturbo.png)
_Captura de pantalla con la consulta en Overtapass Turbo._

El mapa resultante es interactivo, y al seleccionar cualquiera de los nodos resultantes se nos muestra la información existente sobre ese punto en concreto:

![captura mapa overpassturbo](/images/autoreparacionbicisnode.png)
_Captura con el texto que aparece al seleccionar uno de los nodos de la consulta. Abierto 24 horas, 7 dias a la semana, servicio operado por el Ayuntamiento de Burgos, tiene bomba de aire y herramientas para bicicletas._

En este [enlace](https://overpass-turbo.eu/s/1Dut) se puede ejecutar la consulta, aquí os la dejo escrita:

{{< highlight java >}}
[out:json][timeout:25];
// seleccion de la etiqueta "bicycle_repair_station"
node["amenity"="bicycle_repair_station"]
(42.300293723234,-3.7549209594727,42.389233630174,-3.6100387573242);

// mostrar los resultados
out geom;
{{< /highlight >}}

### Añadir estilos:

Como [Overpass turbo soporta MapCSS](https://wiki.openstreetmap.org/wiki/Overpass_turbo/MapCSS) se pueden definir diferentes estilos a las consultas, por ejemplo poner iconos de estación de reparaciones:

![Iconos en overpass turbo](/images/overpassIconos.png)

Texto que hay que añadir a la consulta:

{{< highlight java >}}
{{style:

    node[amenity=bicycle_repair_station] {
        icon-image: url('icons/osmic/repair-bicycle-18.png');
        icon-width: 28;
    }

}}
{{< /highlight >}}

Overpass turbo usa iconos de maki, mapnik y osmic, solo hay que buscar el que nos interesa en [github.com/tyrasd/overpass-turbo/icons](https://github.com/tyrasd/overpass-turbo/tree/master/icons) y poner el enlace.

En la sección de [enlaces](https://biciburgos.gitlab.io/enlaces/) hay varias consultas interesantes a Overpass (aparcamientos de bicicletas, tiendas de bicis, etc)
