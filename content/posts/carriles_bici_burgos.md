+++
title = 'Carriles bici en Burgos'
date = 2023-10-02T20:37:28+02:00
draft = false
+++

Para ver los carriles bici disponibles en la actualidad, el propio [Ayuntamiento de Burgos](https://movilidad.aytoburgos.es/bici 'Web Ayto Burgos / bici') nos remite desde su web al mapa de [OpenStreetMap](https://www.openstreetmap.org/?mlat=42.3405&mlon=-3.6996#map=15/42.3405/-3.6996&layers=Y 'mapa de OpenStreetMap').

Sin embargo, la mejor manera de ver los carriles bici y poder planificar tu ruta es usando el [mapa de CyclOSM](https://www.cyclosm.org/#map=13/42.3479/-3.6751/cyclosm 'http://www.cyclosm.org/'), que usa los datos de OpenStreetMap, pero los muestra de una manera mas orientada a las rutas en bici.

![Logo CyclOSM](/images/cyclosm.png)
