+++
title = 'About'
date = 2023-10-02T18:56:32+02:00
draft = false
+++

Web creada por BiciBurgos con el objetivo de aglutinar datos y enlaces de interes para toda aquella persona que quiera moverse por la ciudad de Burgos en bicicleta.

Si lo deseas puedes contactarme en Mastodon:

<a rel="me" href="https://masto.ai/@biciBurgos">@BiciBurgos@masto.ai</a>

<img src="/images/biciBurgos100.png" alt="BiciBurgosLogo" style="margin-left:0px;">
